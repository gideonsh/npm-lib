# orschweitzer/slugger

[![marker](https://badgen.net/npm/v/@orschweitzer/slugger)](https://www.npmjs.com/package/@orschweitzer/slugger)
[![jest](https://jestjs.io/img/jest-badge.svg)](https://github.com/facebook/jest)
[![jest](https://img.shields.io/bundlephobia/min/@orschweitzer/slugger.svg)](https://www.npmjs.com/package/@orschweitzer/slugger)

A simple function that divide words with '-'

## installation
```
npm i -D @orschweitzer/slugger
```
## ES6 modules
```javascript
import slugger from '@orschweitzer/slugger'; 
```

## Usage
```javascript
slugger("hello world","my name","is "); // output : hello-world-my-name-is
```
