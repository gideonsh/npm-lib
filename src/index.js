




export function slugger(...args) {
    const res = args.reduce((acc, item, index) => {
    acc = index === 0 ? item.split(" ").join("-") : acc + "-" + item.split(" ").join("-");

    return acc;
    }, "");

    return res;
}

