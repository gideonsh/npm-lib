import { slugger } from "./index.js";

/**
 * @describe [optional] - group of tests with a header to describe them
 */
 describe('testing slugger basic functionality', () => {
    /**
     * @it - unit tests can use the 'it' syntax
     */
    it('slugger can slug string with spaces', () => {
        expect(slugger("i am slugged")).toEqual("i-am-slugged");
    })
    /**
     * @test - unit test can use the 'test' syntax
     */
    test('slugger can slug any number of spacy strings', () => {
        expect(slugger("i","am","slugged")).toEqual("i-am-slugged");
    })
})




